describe("Add/remove ticker", () => {
    it("should add a Novartis ticker when clicking the NOVN button", () => {
      cy.visit("/");
      cy.get('[data-ticker="NOVN"]')
      .click();
      cy.get('mat-grid-tile').should('have.length',1);
    });

    it("should remove a Roche ticker when clicking the Remove button", () => {
          cy.visit("/");
          cy.get('[data-ticker="ROG"]')
          .click();
          cy.get('mat-grid-tile').within(($tile)=> {
            cy.get('button')
            .click()
          });
          cy.get('mat-grid-tile').should('have.length',0);
        });
  });

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface StockPrice {
  ticker: string;
  exchange: string;
  timestamp: number;
  price: number;
}

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {

  @Input() price: StockPrice;
  @Output() remove: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  click(): void {
    this.remove.emit(this.price.ticker);
  }
}

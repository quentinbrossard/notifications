import { OnDestroy } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
@Injectable({
  providedIn: 'root'
})
export class WebsocketService implements OnDestroy {
  webSocketEndPoint = 'ws://localhost:8080';
  topic = '/stockprices';
  connection$: WebSocketSubject<any>;
  connect(): Observable<any> {

    if (this.connection$) {
      return this.connection$;
    } else {
      this.connection$ = webSocket(this.webSocketEndPoint + this.topic);
      return this.connection$;
    }
  }

  send(data: any): void {
    if (this.connection$) {
      this.connection$.next(data);
    } else {
      console.error('Did not send data, open a connection first');
    }
  }
  closeConnection(): void {
    if (this.connection$) {
      this.connection$.complete();
      this.connection$ = null;
    }
  }
  ngOnDestroy(): void {
    this.closeConnection();
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WebSocketSubject } from 'rxjs/webSocket';
import { StockPrice } from '../price/price.component';
import { WebsocketService } from '../websocket.service';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss']
})
export class PricesComponent implements OnInit, OnDestroy {

  tickers = ['NOVN', 'GIVN', 'ROG', 'SGSN', 'UHR', 'SLHN',
    'ZURN', 'NESN', 'LHN', 'ABBN', 'GEBN', 'SREN', 'BAER', 'SIKA', 'CSGN', 'UBSG', 'ADEN', 'LONN', 'CFR', 'SCMN'];

  prices: StockPrice[] = [];
  destroyed$ = new Subject();
  messageCount = 0;
  constructor(private webSocket: WebsocketService) { }

  ngOnInit(): void {
    this.webSocket.connect().pipe(
      takeUntil(this.destroyed$)
    ).subscribe(msg => {
      this.addOrUpdatePrice(msg as StockPrice);
      this.messageCount++;
    },
      error => console.log(error));
  }

  addOrUpdatePrice(price: StockPrice): void {
    const index = this.prices.findIndex(p => p.ticker === price.ticker);
    if (index < 0) {
      this.prices.push(price);
    }
    else {
      this.prices.splice(index, 1, price);
    }
  }

  register(ticker: string): void {
    this.addOrUpdatePrice({ ticker, exchange: '', price: 0.0, timestamp: Date.now() });
    this.webSocket.send({action: 'add', ticker});
  }

  removeCard($event): void {
    this.webSocket.send({action: 'remove', ticker: $event});
    this.prices = this.prices.filter(price => $event !== price.ticker);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}

package li.brossard.demos.notifications;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api")
public class InfoController {
    @GetMapping("info")
    public Info info(){
        return new Info();
    }
}

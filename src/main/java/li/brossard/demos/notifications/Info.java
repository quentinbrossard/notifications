package li.brossard.demos.notifications;

import java.time.LocalDateTime;

public class Info {
    private final LocalDateTime time = LocalDateTime.now();

    public LocalDateTime getTime() {
        return time;
    }

}

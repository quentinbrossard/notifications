package li.brossard.demos.notifications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class StockPriceGenerator {
    private final Logger log = LoggerFactory.getLogger(StockPriceGenerator.class);
    private final  ConcurrentHashMap<String, StockPrice> lastPrices = new ConcurrentHashMap<>();
    private final List<String> tickers = Arrays.asList("NOVN","GIVN","ROG","SGSN","UHR","SLHN","ZURN",
            "NESN","LHN","ABBN","GEBN","SREN","BAER","SIKA","CSGN","UBSG","ADEN","LONN","CFR","SCMN");
    private final StockPriceHandler stockPriceHandler;

    public StockPriceGenerator(StockPriceHandler stockPriceHandler) {
        this.stockPriceHandler = stockPriceHandler;
    }

    @Scheduled(fixedRate = 2000L)
    public void price() throws Exception {
        log.debug("generating prices");
        SecureRandom random = new SecureRandom();
        // Thread.sleep(random.nextInt(5000));
        for (String ticker: tickers) {
            // random walk for the price
            BigDecimal step = BigDecimal.valueOf(-5).add(BigDecimal.valueOf(random.nextInt(11)));
            step = step.divide(BigDecimal.valueOf(10L), MathContext.DECIMAL32);
            StockPrice newPrice = lastPrices.getOrDefault(ticker, StockPrice.of(ticker)).priceMove(step);
            lastPrices.put(newPrice.getTicker(), newPrice);
            stockPriceHandler.notifyPrices(newPrice);
        }
    }
}

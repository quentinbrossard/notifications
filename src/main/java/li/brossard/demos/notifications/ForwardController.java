package li.brossard.demos.notifications;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ForwardController {
    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index.html");
    }
    @GetMapping(value = {"/home"})
    public String frontend() {
        return "forward:/";
    }
}
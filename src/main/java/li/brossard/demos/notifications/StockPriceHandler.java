package li.brossard.demos.notifications;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * WebSocket handler to send stock prices.
 * After connecting, the client must send a text message with the ticker
 * for which they want price updates (see {@link #handleTextMessage(WebSocketSession, TextMessage)}).
 * The prices must be notify from the producer using {@link #notifyPrices(StockPrice)}
 */
public class StockPriceHandler extends TextWebSocketHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(StockPriceHandler.class);
    private final List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    private final ObjectMapper mapper = new ObjectMapper();
    private final Map<String, Set<WebSocketSession>> sessionsWithTicker = new ConcurrentHashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
        LOGGER.info("new session connected, total sessions: " + sessions.size());
        super.afterConnectionEstablished(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
        LOGGER.info("session disconnected, total sessions: " + sessions.size());
        // deregister the closed session
        for (Map.Entry<String, Set<WebSocketSession>> entry : sessionsWithTicker.entrySet()) {
            entry.getValue().remove(session);
        }
        super.afterConnectionClosed(session, status);
    }

    /**
     * Registers a session for price updates for a specific stock ticker.
     *
     * @param session the session
     * @param message the ticker string
     * @throws Exception per contract.
     */
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<>() {};
        Map<String, String> messageMap = mapper.readValue(message.getPayload(),typeRef);
        String ticker = messageMap.get("ticker");
        String action = messageMap.get("action");
        if ("add".equals(action)) {
            registerSessionTicker(ticker, session);
        } else if ("remove".equals(action)) {
            unregisterSessionTicker(ticker, session);
        } else {
            LOGGER.error("invalid command provided, allowed values are 'add' or 'remove'");
        }
    }

    private void registerSessionTicker(String ticker, WebSocketSession session) {
        Set<WebSocketSession> sessionsForTicker = sessionsWithTicker.getOrDefault(ticker, new HashSet<>());
        sessionsForTicker.add(session);
        sessionsWithTicker.put(ticker, sessionsForTicker);
    }

    private void unregisterSessionTicker(String ticker, WebSocketSession session) {
        Set<WebSocketSession> sessionsForTicker = sessionsWithTicker.get(ticker);
        if (!sessionsForTicker.isEmpty()) {
            sessionsForTicker.remove(session);
            sessionsWithTicker.put(ticker, sessionsForTicker);
        }
    }

    public void notifyPrices(StockPrice price) {
        // here we only notify sessions that register for the specific ticker.
        sessionsWithTicker.getOrDefault(price.getTicker(), Collections.emptySet()).forEach(webSocketSession -> {
            try {
                webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(price)));
            } catch (IOException e) {
                LOGGER.error("Error occurred.", e);
            }
        });
    }
}

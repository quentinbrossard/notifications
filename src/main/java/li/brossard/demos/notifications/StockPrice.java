package li.brossard.demos.notifications;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.LocalDateTime;

public class StockPrice {
    private final LocalDateTime timestamp = LocalDateTime.now();
    private final String ticker;
    private final String exchange;
    private final BigDecimal price;

    public StockPrice(String ticker, String exchange, BigDecimal price) {
        this.ticker = ticker;
        this.exchange = exchange;
        this.price = price;
    }

    public static StockPrice of(String ticker) {
    return new StockPrice(ticker, "SIX", BigDecimal.valueOf(new SecureRandom().nextInt(1000)));
    }

    public String getTicker() {
        return ticker;
    }

    public String getExchange() {
        return exchange;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @JsonIgnore
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public String getTimestampString() {
        return timestamp.toString();
    }

    public StockPrice priceMove(BigDecimal step) {
        return new StockPrice(this.ticker, this.exchange, this.price.add(step));
    }
}

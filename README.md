# Getting Started

A minimalistic example of Spring Boot + Angular Websocket Integration.

Also includes cypress for e2e tests.

### Reference Documentation
For further reference, please consider the following sections:

* [WebSocket](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-websockets)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Using WebSocket to build an interactive web application](https://spring.io/guides/gs/messaging-stomp-websocket/)
